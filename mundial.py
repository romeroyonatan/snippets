import requests

juegos = requests.get("http://worldcup.sfg.io/matches").json()
for juego in juegos:
    if juego["status"] in ("completed", "in progress"):
        print(juego["home_team"]["country"],
              juego["home_team"]["goals"],
              juego["away_team"]["goals"],
              juego["away_team"]["country"],
              juego["status"])
