import asyncio


async def coroutine_1():
    print("Corutina 1: Iniciando")
    print("Corutina 1: Voy a dormir 4 segundos")
    await asyncio.sleep(4)
    print("Corutina 1: Ya me desperte, voy a terminar la ejecucion")


async def coroutine_2():
    print("Corutina 2: Iniciando")
    print("Corutina 2: Voy a dormir 5 segundos")
    await asyncio.sleep(5)
    print("Corutina 2: Ya me desperte, voy a terminar la ejecucion")


# this is the event loop
loop = asyncio.get_event_loop()

# schedule both the coroutines to run on the event loop
loop.run_until_complete(asyncio.gather(coroutine_1(), coroutine_2()))
