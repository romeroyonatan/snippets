import asyncio
import aiohttp


async def print_headers(url):
    print("Empezando peticion a", url)
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            print("*" * 100)
            print(url, "=== Headers: ", response.headers)
            text = await response.text()
            return text


async def handle_requests():
    urls = ("https://httpbin.org/get", "https://dr.allright.com.ar/",
            "https://twitter.com",)
    print("Handler: Comenzando con las peticiones")
    tasks = [asyncio.ensure_future(print_headers(url)) for url in urls]
    print("Handler: Espero que terminen las peticiones", tasks)
    await asyncio.wait(tasks)
    print("Handler: Terminado")


asyncio.get_event_loop().run_until_complete(handle_requests())
