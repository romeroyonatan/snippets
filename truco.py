"""Implementa reglas del Truco Argentino.

Requiere de Python 3.6+

>>> mazo = Mazo()
>>> mano = mazo[10], mazo[25], mazo[38]
>>> mano[1] > mano[0]
False


Puedo mezclar el mazo usando la funcion built-in ``shuffle``

>>> import random
>>> random.shuffle(mazo)


Puedo calcular los tantos de envido utilizando funciones built-ins

>>> import itertools
>>> max(x.tantos(y) for x, y in itertools.combinations(mano, 2))
0
"""
import enum
import reprlib


class Palo(enum.Enum):
    """Palos de la baraja española

    >>> Palo.BASTO.name
    'BASTO'
    >>> Palo.BASTO.value
    'BASTO'
    """
    BASTO = "BASTO"
    COPA = "COPA"
    ESPADA = "ESPADA"
    ORO = "ORO"


class Carta:
    """Carta de la baraja española

    Ejemplos
    ----------------------------
    >>> Carta(7, Palo.ORO)
    Carta(numero=7, palo=Palo.ORO)
    >>> Carta(10, Palo.BASTO)
    Carta(numero=10, palo=Palo.BASTO)
    """
    # implemento los atributos como slots para ahorrar memoria y tiempo de
    # acceso
    __slots__ = ("numero", "palo")

    def __init__(self, numero, palo):
        """Crea una nueva carta.

        Ejemplos
        ----------------------------
        >>> Carta(7, Palo.ORO)
        Carta(numero=7, palo=Palo.ORO)
        >>> Carta(10, Palo.BASTO)
        Carta(numero=10, palo=Palo.BASTO)
        """
        self.palo = palo
        self.numero = numero

    def __str__(self):
        """Devuelve representacion amigable al usuario/a.

        Ejemplos
        ----------------------------
        >>> carta = Carta(10, Palo.BASTO)
        >>> print(f"Tengo un {carta}")
        Tengo un 10 de BASTO
        """
        return f"{self.numero} de {self.palo.name}"

    def __repr__(self):
        """Devuelve representacion amigable al desarrollador/a.

        Ejemplos
        ----------------------------
        >>> carta = Carta(10, Palo.BASTO)
        >>> repr(carta)
        'Carta(numero=10, palo=Palo.BASTO)'
        """
        return f"Carta(numero={self.numero!r}, palo={self.palo})"

    def __eq__(self, other):
        """Devuelve True si dos cartas son del mismo numero y del mismo
        palo

        Ejemplos
        ----------------------------
        >>> Carta(10, Palo.BASTO) == Carta(10, Palo.ORO)
        False
        >>> Carta(10, Palo.BASTO) == Carta(10, Palo.BASTO)
        True
        """
        return self.palo == other.palo and self.numero == other.numero

    def __hash__(self):
        """Usado para comparar claves en diccionario.

        Si dos objetos son iguales, deben compartir el mismo hash.

        Ejemplos
        ----------------------------
        >>> carta1 = Carta(10, Palo.BASTO)
        >>> carta2 = Carta(10, Palo.BASTO)
        >>> carta3 = Carta(10, Palo.COPA)
        >>> id(carta1) != id(carta2)  # son distintas instancias
        True
        >>> carta1 == carta2
        True
        >>> hash(carta1) == hash(carta2)
        True
        >>> hash(carta1) != hash(carta3)
        True
        """
        return hash(self.palo) ^ hash(self.numero)

    def __lt__(self, other):
        """Devuelve True si la carta es de menor peso que other respetando
        las reglas del Truco Argentino.

        Ejemplos
        ----------------------------
        >>> ancho_basto = Carta(1, Palo.BASTO)
        >>> falso_ancho = Carta(1, Palo.ORO)
        >>> ancho_espada = Carta(1, Palo.ESPADA)
        >>> ancho_basto < falso_ancho
        False
        >>> ancho_basto < ancho_espada
        True
        """
        return self.peso < other.peso

    def __gt__(self, other):
        """Devuelve True si la carta es de mayor peso que other respetando
        las reglas del Truco Argentino.

        Ejemplos
        ----------------------------
        >>> ancho_basto = Carta(1, Palo.BASTO)
        >>> falso_ancho = Carta(1, Palo.ORO)
        >>> ancho_espada = Carta(1, Palo.ESPADA)
        >>> ancho_basto > falso_ancho
        True
        >>> ancho_basto > ancho_espada
        False
        """
        return self.peso > other.peso

    @property
    def peso(self):
        """Devuelve el peso relativo de la carta segun las reglas del truco.

        La escala en el Truco Argentino es (de menor a mayor):

        1. Todos los 4
        2. Todos los 5
        3. Todos los 6
        4. 7 de basto y 7 de copa
        5. Todos los 10
        6. Todos los 11
        7. Todos los 12
        8. 1 de oro y 1 de copa
        9. Todos los 2
        10. Todos los 3
        11. 7 de oro
        12. 7 de espada
        13. 1 de basto
        14. 1 de espada

        Ejemplos
        ----------------------------
        >>> ancho_basto = Carta(1, Palo.BASTO)
        >>> falso_ancho = Carta(1, Palo.ORO)
        >>> ancho_espada = Carta(1, Palo.ESPADA)
        >>> ancho_basto.peso
        13
        >>> falso_ancho.peso
        8
        >>> ancho_espada.peso
        14
        """
        if self in las_pulentas:
            return las_pulentas[self]
        return pesos[self.numero]

    @property
    def puntos_envido(self):
        """Devuelve los puntos de envido que posee la carta.

        Generalmente estos puntos coinciden con el numero de la carta, con
        excepcion del 10, 11 y 12 que es cero

        Ejemplos
        ----------------------------
        >>> siete_oro = Carta(7, Palo.ORO)
        >>> caballo = Carta(11, Palo.ORO)
        >>> siete_oro.puntos_envido
        7
        >>> caballo.puntos_envido
        0

        :returns: int entre 0 y 7
        """
        if self.numero in (10, 11, 12):
            return 0
        return self.numero

    def tantos(self, other):
        """Devuelve la cantidad de puntos de envido de las dos cartas.

        Para tener tantos de envido es necesario que las dos cartas sean
        del mismo palo.

        Si las dos cartas son del mismo palo, se suman los puntos de envido
        de cada carta más 20

        Si las cartas son de distinto palo, devuelve 0


        Ejemplos
        ----------------------------
        >>> ancho_basto = Carta(1, Palo.BASTO)
        >>> siete_oro = Carta(7, Palo.ORO)
        >>> caballo_oro = Carta(11, Palo.ORO)

        >>> siete_oro.tantos(caballo_oro)
        27
        >>> siete_oro.tantos(ancho_basto)
        0
        >>> caballo_oro.tantos(ancho_basto)
        0
        >>> caballo_oro.tantos(siete_oro)
        27

        :returns: int entre 0 y 33
        """
        if self.palo != other.palo:
            return 0
        return 20 + self.puntos_envido + other.puntos_envido


class Mazo:
    """Conjunto de cartas para jugar al Truco Argentino

    Ejemplos
    ----------------------
    >>> mazo = Mazo()
    >>> mazo[0:2]
    [Carta(numero=1, palo=Palo.BASTO), Carta(numero=2, palo=Palo.BASTO)]
    """

    def __init__(self, cartas=None):
        """Inicializa mazo con las cartas pasadas por parametro.

        Si no se pasa ninguna carta, se crea un nuevo conjunto de cartas

        Ejemplos
        ----------------------
        >>> Mazo()
        'Mazo([Carta(...o=Palo.ORO)])'

        >>> Mazo([])
        'Mazo([])'

        >>> Mazo(Carta(numero, Palo.ESPADA) for numero in range(0, 8))
        'Mazo([Carta(...alo.ESPADA)])'
        """
        if cartas is None:
            self._cartas = [
                Carta(numero, palo)
                for palo in Palo
                for numero in range(1, 13)
                if numero not in (8, 9)
            ]
        else:
            self._cartas = list(cartas)

    def __repr__(self):
        """Devuelve representacion amigable al desarrollador/a

        Ejemplos
        ----------------------------------------------
        >>> Mazo()
        'Mazo([Carta(...o=Palo.ORO)])'

        >>> Mazo([])
        'Mazo([])'
        """
        return reprlib.repr(f"{self.__class__.__name__}({self._cartas!r})")

    def __len__(self):
        """Devuelve la cantidad de cartas en el mazo

        Ejemplos
        -------------------------
        >>> mazo = Mazo()
        >>> len(mazo)
        40
        """
        return len(self._cartas)

    def __getitem__(self, index: int):
        """Obtiene una carta por indice

        Ejemplos
        -------------------------
        >>> mazo = Mazo()
        >>> mazo[0]
        Carta(numero=1, palo=Palo.BASTO)
        """
        return self._cartas[index]

    def __setitem__(self, key: int, value: Carta):
        """Asigna una carta en el indice

        Ejemplos
        -------------------------
        >>> mazo = Mazo()
        >>> mazo[0]
        Carta(numero=1, palo=Palo.BASTO)
        >>> mazo[0] = Carta(7, Palo.ORO)
        >>> mazo[0]
        Carta(numero=7, palo=Palo.ORO)
        """
        self._cartas[key] = value


# Estas son las cartas mas poderosas del juego
las_pulentas = {
    Carta(1, Palo.ESPADA): 14,
    Carta(1, Palo.BASTO): 13,
    Carta(7, Palo.ESPADA): 12,
    Carta(7, Palo.ORO): 11,
}

# Esta es la escala de peso en el juego, no importa el palo, sino solo
# el numero (por eso las pulentas son especiales)
pesos = {
    3: 10,
    2: 9,
    1: 8,
    12: 7,
    11: 6,
    10: 5,
    7: 4,
    6: 3,
    5: 2,
    4: 1,
}
