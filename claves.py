"""Permite codificar texto plano en alguna clave scout

Las claves soportadas son

* morse
* murcielago
"""
import functools


MURCIELAGO = {
    "m": "0",
    "u": "1",
    "r": "2",
    "c": "3",
    "i": "4",
    "e": "5",
    "l": "6",
    "a": "7",
    "g": "8",
    "o": "9",
}

MORSE = {
    "a": ".-",
    "b": "-...",
    "c": "-.-.",
    "d": "-..",
    "e": ".",
    "f": "..-.",
    "g": "--.",
    "h": "....",
    "i": "..",
    "j": "----",
    "k": "-.-",
    "l": ".-..",
    "m": "--",
    "n": "-.",
    "ñ": "--.--",
    "o": "---",
    "p": ".--.",
    "q": "--.-",
    "r": ".-.",
    "s": "...",
    "t": "-",
    "u": "..-",
    "v": "...-",
    "w": ".-.",
    "x": "-..-",
    "y": "..--",
    "z": "--..",
    " ": "   ",
    "1": ".----",
    "2": "..---",
    "3": "...--",
    "4": "....-",
    "5": ".....",
    "6": "-....",
    "7": "--...",
    "8": "---..",
    "9": "----.",
    "0": "-----",
}


def codificar(texto, alfabeto, separador=""):
    """Retorna el texto codificado por reemplazo simple utilizando el alfabeto

    Ejemplos
    --------------
    >>> codificar("murcielago", MURCIELAGO)
    '0123456789'

    >>> codificar("GALOPE", MURCIELAGO)
    '8769P5'

    >>> codificar("hola", MORSE, separador=" ")
    '.... --- .-.. .-'
    """
    return separador.join(
        alfabeto.get(letra.lower(), letra) for letra in texto
    )


morse = functools.partial(codificar, alfabeto=MORSE, separador=" ")
murcielago = functools.partial(codificar, alfabeto=MURCIELAGO)


if __name__ == "__main__":
    import sys
    import argparse

    parser = argparse.ArgumentParser(
        """Permite codificar texto plano en alguna clave scout"""
    )
    parser.add_argument(
        "--morse", action="store_true", help="Codificar mensaje en morse"
    )
    parser.add_argument(
        "--murcielago",
        action="store_true",
        help="Codificar mensaje en murcielago (Por defecto)",
    )
    args = parser.parse_args()

    texto = sys.stdin.read()

    if args.morse:
        print(morse(texto))
    else:
        print(murcielago(texto))
